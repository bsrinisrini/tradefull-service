<?php
namespace App\Http\Controllers;
use App\Models\Products;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProductController extends Controller
{
    public function __construct()
    {
        // $this->middleware('name');
    }

    public function index()
    {
        $products = Products::where('status', 1)
            ->orderBy('name')
            ->take(10)
            ->get();
        return $products;
    }

    public function store(Request $request)
    {
        $product = new Products;
        $product->name = $request->post('name');
        $product->description = $request->post('description');
        $product->price = $request->post('price');
        $product->image_path = $request->post('image_path');
        $product->save();
        return array('success' => true, 'message' => 'product saved');
    }

    public function show($id)
    {
        $product = Products::where('id', $id)->first();
        return $product;
    }

    public function update(Request $request, $id)
    {
        $product = Products::find($id);
        if (!empty($request->post('name'))) {
            $product->name = $request->post('name');
        }
        if (!empty($request->post('description'))) {
            $product->description = $request->post('description');
        }
        if (!empty($request->post('price'))) {
            $product->price = $request->post('price');
        }
        if (!empty($request->post('image_path'))) {
            $product->image_path = $request->post('image_path');
        }
        $product->save();
        return array('success' => true, 'message' => 'product updated');
    }

    public function destroy($id)
    {
        $product = Products::find($id);
        $product->delete();
        return array('success' => true, 'message' => 'product deleted');
    }

}
